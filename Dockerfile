FROM alpine:edge

RUN apk add --update --no-cache alpine-sdk bash python cmake && \
    curl https://codeload.github.com/edenhill/kafkacat/tar.gz/master | tar xzf - && \
    cd kafkacat-* && \
    bash ./bootstrap.sh && \
    apk del alpine-sdk bash python cmake && \
    rm -rf /var/cache/apk/* && \
    mv /kafkacat-*/kafkacat /usr/bin/kafkacat && \
    rm -rf /kafkacat-*

ENTRYPOINT ["kafkacat"]
