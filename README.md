 examples
- Consume from topic
  * docker run --rm registry.gitlab.com/frankbohman/kafkacat -C -b broker:9092 -o beginning -t test-topic
  * docker run --rm registry.gitlab.com/frankbohman/kafkacat -C -b broker:9092 -o -1 -t test-topic
  * docker run --rm registry.gitlab.com/frankbohman/kafkacat -C -b broker:9092 -o end -t test-topic
- List topics
  * docker run --rm registry.gitlab.com/frankbohman/kafkacat -L
